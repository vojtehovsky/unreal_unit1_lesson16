#pragma warning(disable : 4996)
#include <iostream>
#include <iomanip>
#include <ctime>

int getCurrentDayOfMonth() {
    const time_t now = time(0);
    tm* ltm = localtime(&now);
    return ltm->tm_mday;
}

int main() {
    const int dayOfMonth = getCurrentDayOfMonth();
    std::cout << "Day of month: " << dayOfMonth << std::endl;

    const int N = 10;
    int array[N][N];
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            array[i][j] = i + j;
        }
    }

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            std::cout << std::setfill(' ') << std::setw(3) << array[i][j] << " ";
        }
        std::cout <<"\n";
    }

    int line = dayOfMonth % N;
    int sum = 0;
    for (int i = 0; i < N; i++) {
        sum += array[line][i];
    }
    std::cout << "Sum of elements for line " << line << " is: " << sum;

}
